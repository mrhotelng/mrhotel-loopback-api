var _ = require("underscore");


module.exports = function(HotelHotelavailability) {
    HotelHotelavailability.searchAvailability = function(check_in,location,duration,number_of_rooms,callback){
        var checkinDate = new Date(check_in);
        //console.log("check date",checkinDate);
        var app = HotelHotelavailability.app
        var HotelRoom = app.models.HotelHotelroom;
        var HotelPlace = app.models.HotelPlace;
        var pattern = new RegExp('.*'+location+'.*', "i"); /* case-insensitive RegExp search */
        var locationArray = location.split(",");
        var xx = checkinDate.getMonth();
        var PLACES = null
        if((xx+1)<10){
            xx = "0"+(xx+1);
        }
        var date = checkinDate.getDate();
        if((date+1)<10){
            date= "0"+date;
        }
        var dateString = checkinDate.getFullYear()+"-"+xx+"-"+date ;
        //console.log("dateString",dateString);
        HotelPlace.find({
            where:{
                or:[{city:{like:'%'+locationArray[0]+'%'}},{state:{like:'%'+locationArray[1]+'%'}}]
            }
        },function(err,places){
            //console.log("places",places)
            if(err){
                console.log(err)
            }
            PLACES = places;
            //it is going to be one result simply because we are searching
            //query the DB to check if for the hotel that has dates available
            app.models.HotelHotelavailability.find({where:{availableDate:{gte:dateString}},include:['hotel','hotelRoom']},function(error,resulter){
                var filteredList1 = [];
                //filter down the list by place_id
                for(var i=0;i<resulter.length;i++){
                    if(resulter[i].hotel().placeId==places[0].id){
                        //get the new image
                        filteredList1.push(resulter[i]);
                    }
                }
               // console.log(filteredList1)
                //create final object
                var objToGo = []
                for(var s=0;s<filteredList1.length;s++){
                    var temp = filteredList1[s];


                    temp.hotel = {}
                    temp.lat = places[0].lat;
                    temp.lon = places[0].lon;
                    temp.hotel = filteredList1[s].hotel();
                    temp.hotelRoom = {}
                    temp.hotelRoom = filteredList1[s].hotelRoom();
                    var tempImage = JSON.stringify(filteredList1[s].hotelRoom().image)
                    var iii = JSON.parse(tempImage)
                    iii = iii.replace(/"(\w+)"\s*:/g, '$1:');

                    temp.image = iii

                    objToGo.push(temp);
                }
             callback(null,filteredList1);
            })
        })
    }
    HotelHotelavailability.remoteMethod('searchAvailability',  {
        accepts: [{arg: 'check_in', type: 'Date'},{arg: 'location', type: 'String'},{arg: 'duration', type: 'String'},{arg: 'number_of_rooms', type: 'String'}],
        returns: { arg: 'data', type: 'Object', http: { source: 'body' } },
        http: {path:'/searchAvailability', verb: 'post'}
    })

};
